.PHONY: all configure doc

all: configure
	ocaml setup.ml -build

doc configure: setup.ml
	ocaml setup.ml -$@



setup.ml:
	oasis setup

clean: 
	ocaml setup.ml -clean

