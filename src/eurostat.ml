type t = {
  name : string ;
  labels : string list ;
  content : (string list, string) Hashtbl.t
}

let empty name labels = {
    name = name ;
    labels = labels ;
    content = Hashtbl.create 17
  }

let add_value db key value = Hashtbl.add db.content key value

let get_name db = db.name

(** {2 Input} *)

let parse_header = function
  | [] | [_] -> assert false
  | h::h_vals ->
      (Str.split (Str.regexp "[,\\]") h, h_vals)
      
let parse_row db h_vals = function
  | [] | [_] -> assert false
  | key_::row ->
      let key = Str.split (Str.regexp ",") key_ in
      List.iter2
        (fun h_val value -> add_value db (key@[h_val]) value)
        h_vals
        row

let from_channel ic name =
  let csv = Csv.of_channel ~separator:'\t' ic in
  let (labels, h_vals) = parse_header (Csv.next csv) in
  let db = empty name labels in
  Csv.iter ~f:(parse_row db h_vals) csv;
  db

let from_file ?(name="") filename =
  let ic = open_in filename in
  let db_name = (
    if name = "" then
      try Filename.chop_suffix filename ".tsv"
      with Invalid_argument _ -> filename
    else name
  ) in
  try
    let db = from_channel ic db_name in
    close_in ic;
    db
  with err -> close_in_noerr ic ; raise err

(** {2 Output} *)

let rec write_sep_list oc sep = function
  | [] -> ()
  | [s] -> Printf.fprintf oc "%s" s
  | t::q -> Printf.fprintf oc "%s%c" t sep; write_sep_list oc sep q  

let to_csv_channel ?(sep=',') oc db = 
  (* Header *)
  write_sep_list oc sep db.labels ;
  Printf.fprintf oc "%c%s\n" sep db.name;
  (* Body *)
  let print_binding key value =
    write_sep_list oc sep key ;
    Printf.fprintf oc "%c%s" sep value ;
    Printf.fprintf oc "\n" in
  Hashtbl.iter print_binding db.content 
