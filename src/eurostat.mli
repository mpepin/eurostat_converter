type t
(** Representation of Eurostat databases *)

val empty : string -> string list -> t
(** [empty name labels] returns a fresh database with name [name] and a list of
    labels [labels]. *)

val add_value : t -> string list -> string -> unit
(** [add_value db key value] adds the value [value] associated to the key [key]
    into the database [db]. *)

val get_name : t -> string
(** Returns the name of a given database *)

(** {2 Input} *)

val from_channel : in_channel -> string -> t
(** [from_channel ic name] reads an eurostat database from the input channel
    [ic] and returns a database with name [name]. *)

val from_file : ?name:string -> string -> t
(** [from_string ~name:"foo" filename] does the same as from_channel but reads
    the database from the file [filename] and names it using the optional
    argument [name]. If [name] is empty or not specified, the filename (without
    the file extension) is used instead. *)

(** {2 Output} *)

val to_csv_channel : ?sep:char -> out_channel -> t -> unit
(** [to_csv_channel ~sep:';' oc db] writes the database in the csv format into
    the channel [oc]. The [sep] argument can be used to specify the separator,
    comma ([',']) is the default. *)
