let ifile = ref ""

let set_str f s = f := s

let options = []

let usage = "usage: converter.byte file.tsv"

let () =
  (* Command line parsing *)
  Arg.parse options (set_str ifile) usage;
  if not (Filename.check_suffix (!ifile) ".tsv") then begin
    Printf.eprintf "The input file must have the .tsv extension\n";
    exit 1
  end;

  (* Output *)
  let db = Eurostat.from_file !ifile in
  let output = (Eurostat.get_name db) ^ "_out.csv" in
  let oc = open_out output in
  begin try
    Eurostat.to_csv_channel oc db ;
    Printf.printf "File %s converted. Output is: %s.\n" !ifile output ;
    close_out oc
  with e -> close_out_noerr oc; raise e end;

  (* Graceful exit *)
  exit 0
